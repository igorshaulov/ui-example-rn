import React, { Component } from 'react'
import { SafeAreaView, StyleSheet } from 'react-native'
import { MAIN_WHITE } from './src/theme/colors'
import FeedScreen from './src/components/FeedScreen'

type Props = {}
export default class App extends Component<Props> {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <FeedScreen />
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: MAIN_WHITE,
  },
})
