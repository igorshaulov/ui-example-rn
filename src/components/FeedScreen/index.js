import React, { PureComponent } from 'react'
import { View, StyleSheet } from 'react-native'
import Header from '../common/Header'
import FeedView from '../FeedView'
import TabBar from '../common/TabBar'
import feedItemsMock from '../../dataSource/FeedItemsMock'

export default class FeedScreen extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      feedItems: feedItemsMock(30),
    }
  }

  render() {
    const { feedItems } = this.state
    return (
      <View style={styles.container}>
        <Header title="Rodman" />
        <FeedView items={feedItems} />
        <TabBar />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})
