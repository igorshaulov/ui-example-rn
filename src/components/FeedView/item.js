import React, { PureComponent } from 'react'
import {
  View, Text, StyleSheet, Platform,
} from 'react-native'
import PropTypes from 'prop-types'
import FeedObjectType from '../../types/FeedObject'
import Icon, {
  ICON_ELLIPSIS, ICON_SHARE, ICON_COMMENT, ICON_HEART,
} from '../common/Icon'
import { PINK, GREY, WHITE } from '../../theme/colors'
import { SHADOW } from '../../theme/styles'

import Avatar from '../common/Avatar'
import Media from './media'

export default class Item extends PureComponent {
  render() {
    const { itemData, onMorePress, onSharePress } = this.props
    const {
      author: { avatarUrl, name },
      postedDate,
      text,
      media,
      commentsCount,
      likesCount,
    } = itemData
    return (
      <View style={[styles.container, Platform.OS === 'android' ? SHADOW : {}]}>
        <View style={styles.header}>
          <Avatar avatarUrl={avatarUrl} />
          <View style={styles.nameAndDateContainer}>
            <Text style={styles.nameText}>{name}</Text>
            <Text style={styles.dateText}>{postedDate}</Text>
          </View>
          <Icon name={ICON_ELLIPSIS} color={GREY} onPress={onMorePress} />
        </View>
        {text && <Text style={styles.text}>{text}</Text>}
        <Media data={media} />
        <View style={styles.footer}>
          <Icon name={ICON_SHARE} color={GREY} onPress={onSharePress} />
          <View style={styles.spacer} />
          <Text style={styles.valueText}>{commentsCount}</Text>
          <Icon name={ICON_COMMENT} color={GREY} />
          <Text style={styles.valueText}>{likesCount}</Text>
          <Icon name={ICON_HEART} color={PINK} />
        </View>
      </View>
    )
  }
}

Item.propTypes = {
  itemData: FeedObjectType.isRequired,
  onMorePress: PropTypes.func.isRequired,
  onSharePress: PropTypes.func.isRequired,
}

const styles = StyleSheet.create({
  container: {
    height: 265,
    flex: 1,
    backgroundColor: WHITE,
    borderRadius: 4,
    marginHorizontal: 20,
    marginBottom: 15,
  },
  header: {
    margin: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  nameAndDateContainer: {
    flex: 1,
    marginLeft: 8,
  },
  nameText: {
    fontSize: 12,
    fontWeight: '500',
  },
  dateText: {
    fontSize: 10,
    color: GREY,
  },
  text: {
    marginHorizontal: 20,
    marginBottom: 20,
  },
  media: {
    flex: 1,
  },
  footer: {
    height: 50,
    marginHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  spacer: {
    flex: 1,
  },
  valueText: {
    color: GREY,
    marginHorizontal: 10,
  },
})
