import React, { PureComponent } from 'react'
import { FlatList, View, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import FeedObjectType from '../../types/FeedObject'
import { SHADOW } from '../../theme/styles'
import FeedItem from './item'

export default class FeedView extends PureComponent {
  // eslint-disable-next-line
  onMorePress = id => {}

  // eslint-disable-next-line
  onSharePress = id => {}

  keyExtractor = item => item.id

  renderItem = ({ item }) => {
    const { id } = item
    const view = (
      <FeedItem
        itemData={item}
        onMorePress={() => {
          this.onMorePress(id)
        }}
        onSharePress={() => {
          this.onSharePress(id)
        }}
      />
    )
    return view
  }

  render() {
    const { items } = this.props
    return (
      <View style={[styles.container, SHADOW]}>
        <FlatList data={items} keyExtractor={this.keyExtractor} renderItem={this.renderItem} />
      </View>
    )
  }
}

FeedView.propTypes = {
  items: PropTypes.arrayOf(FeedObjectType).isRequired,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})
