import React, { PureComponent } from 'react'
import {
  View, Text, Image, StyleSheet,
} from 'react-native'
import PropTypes from 'prop-types'
import { WHITE } from '../../theme/colors'

export default class Media extends PureComponent {
  // eslint-disable-next-line
  urlByIndex = index => this.props.data[index].imageUrl

  render() {
    const {
      data: { length },
    } = this.props
    let content
    if (length < 2) {
      content = <Image style={styles.image} source={{ uri: this.urlByIndex(0) }} />
    } else {
      let more
      if (length > 2) {
        more = <Text style={styles.moreText}>{`+${length - 2} more`}</Text>
      }
      content = (
        <View style={styles.imageContainer}>
          <Image style={styles.image} source={{ uri: this.urlByIndex(0) }} />
          <View style={styles.spacer} />
          <Image style={styles.image} source={{ uri: this.urlByIndex(1) }} />
          {more}
        </View>
      )
    }
    return <View style={styles.container}>{content}</View>
  }
}

Media.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({ imageUrl: PropTypes.string })).isRequired,
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  spacer: {
    flex: 0.01,
  },
  image: {
    flex: 1,
  },
  moreText: {
    color: WHITE,
    fontSize: 20,
    position: 'absolute',
    bottom: 10,
    right: 10,
  },
})
