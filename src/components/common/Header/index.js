import React, { PureComponent } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import Icon, { ICON_BARS, ICON_FIRE } from '../Icon'
import { PINK } from '../../../theme/colors'

export default class Header extends PureComponent {
  render() {
    const { title, onFirePress, onMenuPress } = this.props
    return (
      <View style={styles.container}>
        <Icon name={ICON_BARS} color={PINK} onPress={onMenuPress} />
        <Text style={styles.title}>{title}</Text>
        <Icon name={ICON_FIRE} color={PINK} onPress={onFirePress} />
      </View>
    )
  }
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  onMenuPress: PropTypes.func,
  onFirePress: PropTypes.func,
}

Header.defaultProps = {
  onMenuPress: () => {},
  onFirePress: () => {},
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    height: 90,
    marginHorizontal: 20,
  },
  title: {
    fontWeight: '800',
    fontSize: 20,
  },
})
