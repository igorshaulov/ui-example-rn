import React, { PureComponent } from 'react'
import { View, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import Icon from '../Icon'
import { PINK, GREY } from '../../../theme/colors'

export default class Item extends PureComponent {
  render() {
    const { iconName, isSelected, onPress } = this.props
    const iconColor = isSelected ? PINK : GREY
    const selectedViewStyle = [styles.selectedView]
    if (!isSelected) {
      selectedViewStyle.push(styles.hidden)
    }
    return (
      <View style={styles.container}>
        <Icon name={iconName} color={iconColor} onPress={onPress} />
        <View style={selectedViewStyle} />
      </View>
    )
  }
}

Item.propTypes = {
  iconName: PropTypes.string.isRequired,
  isSelected: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
}

Item.defaultProps = {
  onPress: () => {},
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    width: 50,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  selectedView: {
    backgroundColor: PINK,
    width: 28,
    height: 3,
    borderRadius: 1.5,
  },
  hidden: { opacity: 0 },
})
