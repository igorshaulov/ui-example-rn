import React, { PureComponent } from 'react'
import { View, StyleSheet } from 'react-native'
import { WHITE } from '../../../theme/colors'
import { SHADOW } from '../../../theme/styles'
import { ICON_COMMENT, ICON_HOME, ICON_SUITCASE } from '../Icon'
import Item from './item'

export default class TabBar extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      items: [
        { id: 'chats', iconName: ICON_COMMENT, isSelected: false },
        { id: 'home', iconName: ICON_HOME, isSelected: true },
        { id: 'profile', iconName: ICON_SUITCASE, isSelected: false },
      ],
    }
  }

  onItemPress = (id) => {
    const { items } = this.state
    const newItems = items.map(item => ({ ...item, isSelected: item.id === id }))

    this.setState({ items: newItems })
  }

  getItems = (data) => {
    const result = data.map((item) => {
      const { id } = item
      return (
        <Item
          key={id}
          onPress={() => {
            this.onItemPress(id)
          }}
          {...item}
        />
      )
    })

    return result
  }

  render() {
    const { items } = this.state
    const containerStyle = [styles.container, SHADOW]
    return <View style={containerStyle}>{this.getItems(items)}</View>
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: WHITE,
    height: 55,
    marginHorizontal: 20,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    borderTopRightRadius: 8,
    borderTopLeftRadius: 8,
  },
})
