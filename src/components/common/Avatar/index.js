import React, { PureComponent } from 'react'
import { Image, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import Icon, { ICON_USER } from '../Icon'

export default class Avatar extends PureComponent {
  render() {
    const { avatarUrl } = this.props
    if (avatarUrl) {
      return <Image style={styles.image} source={{ uri: avatarUrl }} />
    }

    return <Icon name={ICON_USER} size={30} />
  }
}

Avatar.propTypes = {
  avatarUrl: PropTypes.string,
}

Avatar.defaultProps = {
  avatarUrl: null,
}

const styles = StyleSheet.create({
  image: {
    width: 30,
    height: 30,
  },
})
