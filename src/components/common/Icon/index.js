import React from 'react'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome'

export const ICON_BARS = 'bars'
export const ICON_FIRE = 'fire'
export const ICON_USER = 'user'
export const ICON_ELLIPSIS = 'ellipsis-h'
export const ICON_SHARE = 'share-alt'
export const ICON_HEART = 'heart'
export const ICON_COMMENT = 'comment'
export const ICON_HOME = 'home'
export const ICON_SUITCASE = 'suitcase'

const IconComponent = ({
  name, color, onPress, size,
}) => (
  <Icon name={name} size={size} color={color} onPress={onPress} />
)

IconComponent.propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string,
  size: PropTypes.number,
  onPress: PropTypes.func,
}

IconComponent.defaultProps = {
  size: 24,
  color: '#000',
  onPress: () => {},
}

export default IconComponent
