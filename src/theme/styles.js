export const SHADOW = {
  shadowColor: '#000',
  shadowRadius: 10,
  shadowOpacity: 0.2,
  elevation: 8,
}
