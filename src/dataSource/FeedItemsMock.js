import moment from 'moment'

const randomInt = max => Math.ceil(Math.random() * max)

export default (count) => {
  const result = []
  for (let i = 0; i < count; i += 1) {
    const postedDate = moment(new Date())
      .subtract(i, 'days')
      .fromNow()
    const item = {
      id: (i + 1).toString(),
      author: {
        name: `Author #${i + 1}`,
        avatarUrl: null,
      },
      media: [],
      text: Math.random() > 0.5 ? 'Some text' : null,
      postedDate,
      commentsCount: randomInt(1000),
      likesCount: randomInt(1000),
    }

    for (let j = 0; j < i + 1; j += 1) {
      item.media.push({
        imageUrl:
          'https://www.telegraph.co.uk/content/dam/Pets/spark/royal-canin/tabby-kitten-small.jpg',
      })
    }

    result.push(item)
  }

  return result
}
