import { PropTypes } from 'prop-types'

export default PropTypes.shape({
  id: PropTypes.string.isRequired,
  author: PropTypes.shape({
    avatarUrl: PropTypes.string,
    name: PropTypes.string.isRequired,
  }),
  media: PropTypes.arrayOf(PropTypes.shape({ imageUrl: PropTypes.string })),
  text: PropTypes.string,
  postedDate: PropTypes.string.isRequired,
  commentsCount: PropTypes.number.isRequired,
  likesCount: PropTypes.number.isRequired,
})
